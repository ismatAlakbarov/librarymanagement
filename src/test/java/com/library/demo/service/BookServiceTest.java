package com.library.demo.service;

import com.library.demo.DemoApplication;
import com.library.demo.models.Book;
import com.library.demo.repo.BookRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BookServiceTest {
    @Autowired
    private BookRepository repo;

    @Autowired
    private BookService bookService;

    @Test
    @Transactional
    public void editBook() {
        Book book = new Book();
        book.setIsbn("ISBN");
        book.setAuthor("Author");
        book.setTitle("Title");

        repo.save(book);

        Book editedBook = new Book();
        editedBook.setId(1L);
        editedBook.setTitle("Effective Java");
        editedBook.setAuthor("Joshua Bloch");
        editedBook.setIsbn("0134685997");

        assertThat(bookService.editBook(editedBook).getTitle()).isEqualTo("Effective Java");
        assertThat(bookService.editBook(editedBook).getAuthor()).isEqualTo("Joshua Bloch");
        assertThat(bookService.editBook(editedBook).getIsbn()).isEqualTo("0134685997");

    }
}

package com.library.demo.service.impl;

import com.library.demo.models.Book;
import com.library.demo.repo.BookRepository;
import com.library.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    BookRepository repo;

    @Override
    public Book editBook(Book editedBook) {
        Book book = repo.getOne(editedBook.getId());
        if (editedBook.getTitle() != null) {
            book.setTitle(editedBook.getTitle());
        } if (editedBook.getAuthor() != null) {
            book.setAuthor(editedBook.getAuthor());
        } if (editedBook.getIsbn() != null) {
            book.setIsbn(editedBook.getIsbn());
        } if (editedBook.getComments() != null) {
            book.getComments().add(editedBook.getComments().get(0));
        }
        repo.save(book);
        return book;
    }
}

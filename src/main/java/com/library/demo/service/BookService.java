package com.library.demo.service;

import com.library.demo.models.Book;

public interface BookService {
    Book editBook(Book book);
}

package com.library.demo.models;


import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;


@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
public class Comment {
    @Id
    @GeneratedValue
    private Long id;

    String comment;

    @Column(name = "date", nullable = false)
    Date date;

    @PrePersist
    protected void prePersist() {
        if (this.date == null) date = new Date();
    }
}

package com.library.demo.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Book {
    @Id @GeneratedValue
    Long Id;

    private String title;
    private String author;
    private String isbn;

    @OneToMany(cascade={CascadeType.ALL})
    List<Comment> comments;
}

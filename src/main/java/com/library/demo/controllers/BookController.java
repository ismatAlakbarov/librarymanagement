package com.library.demo.controllers;

import com.library.demo.models.Book;
import com.library.demo.models.Comment;
import com.library.demo.repo.BookRepository;
import com.library.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BookController {
    @Autowired
    BookRepository repo;

    @Autowired
    BookService service;

    @GetMapping("/")
    public String index() {
        return "redirect:/books";
    }

    @GetMapping("/books")
    public String list(Model model) {
        model.addAttribute("books", repo.findAll());
        return "books/list";
    }

    @GetMapping(value = "/books/form")
    public String form(Model model) {
        model.addAttribute("book", new Book());
        return "books/create";
    }

    @PostMapping(value = "/books")
    public String create(Book book) {
        repo.save(book);
        return"redirect:/books";
    }
    @PatchMapping(value = "/books")
    public String editForm(Book book) {
        service.editBook(book);
        return"redirect:/books";
    }

    @DeleteMapping(value = "/books/{id}")
    public String removeBook(@PathVariable("id") Long id) {
        repo.deleteById(id);
        return "redirect:/books";
    }

    @PatchMapping(value = "/books/{id}/form")
    public ModelAndView editBook(@PathVariable("id") Long id) {
        Book book = repo.getOne(id);
        ModelAndView model = new ModelAndView("books/edit");
        model.addObject("comments",book.getComments());
        Comment newComment = new Comment();
        book.getComments().add(0, newComment);
        model.addObject("book",book);
        return model;
    }
}
